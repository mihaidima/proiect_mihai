# Wantsome - Battleship Game

### by Mihai Dima, Vlad Rozmarin

In acest proiect ne-am propus sa implementam un joc si anume 
[Battleship](https://en.wikipedia.org/wiki/Battleship_(game).

Acest joc este scris ca o aplicatie web, utilizand ca web framework Spark, partea de backend
fiind scrisa in Java, iar elemente simple de HTML si CSS pe partea de frontend.

Pe partea de frontend am folosit Velocity ca templating engine.

Acest joc a fost scris pentru a fi jucat de 2 jucatori pe 2 browsere diferite.

###Functionalitatea jocului pe partea de Backend(Java)

Modelul jocului a fost scris in mai multe clase separate, unele clase avand scrise
si cateva teste pentru a verifica functionalitatea acestora.

Pentru buna functionare a aplicatiei s-au indentificat ca fiind necesare urmatoarele enum-uri si clase:

1. Enum pentru selectarea directiei: Orizontala si Verticala
2. Enum pentru a defini starea jocului: 
    a) Logarea jucatorilor
    b) Amplasarea navelor pe harta
    c) Derularea jocului
    d) Sfarsitul jocului
3. Enum parametrizat pentru fiecare tip de nava.
Paramerii sunt: acronimul folosit pe harta,
numarul de nave permis pe harta si lungimea acesteia.
    a) Aircraft_Carrier
    b) Battleship
    c) Cruiser
    d) Destroyer
    e) Submarine
4. Clasa Coorddiante, o clasa simpla care are ca scop tranformarea valorilor x si y
primte de la jucatori intr-o coordonata ce va fi folosita la amplasarea navelor
sau in a plasa loviturile in timpul jocului.
5. Clasa Ship, unde este difinat structura fiecarei nave din joc. Pentru aceasta
s-au folosit:
    a) Proprietatile: tipul de nava, respectiv pozitia navei pe harta de tip Map ce contine
    coordonatele si daca a fost scufundata sau nu.
    b) Constructor pentru a creea obiectele de tip nava in functie de: tipul de nava,
    coordonata de start, directie si harta de joc unde va fi dispusa.
    c) Metode pentru lovituri si pentru a identifica daca a fost sau nu scufundata.
6. Clasa Player, are ca scop sa faca partea de mmanagement a jucatorului
respectiv partea de management a hartii acestuia cu ajutorul proprietatilor si
metodelor definite. Aceste propietati sunt: nume, harta, amplasarea navelor
si raspunsul la loviturile adversarului. Harta este 2D cu coordonate X si Y. 
Pentru jucatori au fost create cate 2 harti. Una este harta jucatorului unde isi plaseaza
navele, cealalta este o harta ascunsa a jucatorului oponent pentru a alege coordonata unde
doreste sa loveasca.
7. Clasa GameEngine, clasa ce realizeaza managementul jocului.
Acesta este realizat cu ajutorul:
    a) Proprietati:
        - players de tip Map, cu cheia de tip String si valoarea de tip Player ce are ca
        scop sa mentina statusul jocului pentru cei doi jucatori.
        - playerTurn de tip String care are ca scop sa asigurea ca loviturile sa
        alterneze.
    b) Contructor  pentru a crea un joc nou.
    c) Metode:
        - pentru a ajuta jucatorul sa vizualizeze ce nava trebuie sa amplaseze pe harta;
        - pentru amplasarea navelor pe harta;
        - pentru managementul loviturilor;
        - pentru stabilirea progresului jocului, ex: daca este la amplasarea navelor sau la
        sfarsitul acestuia cand unul dintre jucatori a castigat;
        - pentru a vizualiza harta jucatorului respectiv a adversarului, aceasta din urma
        este ascunsa si va fi vizualizata doar pentru zonele in care s-a lovit;
        - si alte metode de ajutor: numele jucatorului, numele oponentului,
        randul jucatorului pentru a lovi.   

###Proiectarea jocului(Frontend)

   Pe partea de frontend au fost create mai multe pagini ale jocului in functie de starea acestuia
(battleship,login,wait,prepare,play,end).
   Prima pagina este cea de login, unde jucatorul isi alege numele. Dupa ce a ales numele aceasta va fi redirectionat
catre a doua pagina, cea de asteptare, unde jucatorul asteapta un alt 
jucator sa intre in joc.
   In cea de a 3 a pagina este stagiul de pregatire a jocului, unde jucatorii isi plaseaza navele pe harta. In momentul in
care ambii jucatori au plasat toate navele vor fi redirectionati catre pagina urmatoarea unde va fi afisat jocul in sine.
Aceasta pagia va contine harta jucatorului, harta ascunsa a oponentului, starea jocului, randul carui jucator este si
numarul de nave ramase in joc. In momentul in care jucatorul are dreptul de a lovi se va debloca harta adversarului si
acesta va alege coordonata unde doreste sa plaseze lovitura.
   In momentul in care unul dintre jucatori a scufundat toate navele oponentului ambii jucatori vor fi redirectionati
catre ultima pagina unde va fi afisat numele jucatorului care a castigat si hartile celor doi jucatori.

###Regulile jocului

Acest joc se poate juca doar in 2 persoane. Se creeaza o bucla pentru a permite jucatorilor
sa atace o singura data pe runda. Daca un jucator ramane fara nave, jocul se termina si pierde
jocul.