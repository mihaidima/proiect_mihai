package wantsome.project.game_model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlayerTest {

    @Test
    public void shouldFail() {
        try {
            Player p1 = new Player("X < 0");
            Coordinate c1 = new Coordinate(-1, 8);
            p1.createShips(ShipType.AIRCRAFT_CARRIER, c1, Direction.HORIZONTAL);
        } catch (Exception e) {
            System.out.println("Test_1: X < 0");
            System.out.println("Test throw" + e + " exception!");
            System.out.println("Because the given coordinates was: X = " + -1);
            System.out.println();
        }

        try {
            Player p1 = new Player("X > 10");
            Coordinate c1 = new Coordinate(11, 8);
            p1.createShips(ShipType.AIRCRAFT_CARRIER, c1, Direction.HORIZONTAL);
        } catch (Exception e) {
            System.out.println("Test_2: X > 10");
            System.out.println("Test throw Runtime exception!");
            System.out.println("Because the given coordinates was: X = " + 11);
            System.out.println();
        }

        try {
            Player p1 = new Player("Y < 0");
            Coordinate c1 = new Coordinate(0, -18);
            p1.createShips(ShipType.AIRCRAFT_CARRIER, c1, Direction.HORIZONTAL);
        } catch (Exception e) {
            System.out.println("Test_3: Y < 0");
            System.out.println("Test throw Runtime exception!");
            System.out.println("Because the given coordinates was: Y = " + -18);
            System.out.println();
        }

        try {
            Player p1 = new Player("Y > 10");
            Coordinate c1 = new Coordinate(5, 18);
            p1.createShips(ShipType.AIRCRAFT_CARRIER, c1, Direction.HORIZONTAL);
        } catch (Exception e) {
            System.out.println("Test_4: Y > 10");
            System.out.println("Test throw Runtime exception!");
            System.out.println("Because the given coordinates was: Y = " + 18);
        }
    }

    @Test
    public void createAndRemainingShips() {
        Player p1 = new Player("Gigi");
        assertEquals(0, p1.remainingShips());
        p1.createShips(ShipType.AIRCRAFT_CARRIER, new Coordinate(2, 4), Direction.HORIZONTAL);
        assertEquals(1, p1.remainingShips());
    }

    @Test
    public void getPlayerBoard() {
        Player p1 = new Player("Gigi");
        p1.createShips(ShipType.AIRCRAFT_CARRIER, new Coordinate(2, 4), Direction.HORIZONTAL);
        System.out.println(p1.remainingShips());

        p1.createShips(ShipType.BATTLESHIP, new Coordinate(1, 5), Direction.HORIZONTAL);
        System.out.println(p1.remainingShips());

        p1.createShips(ShipType.CRUISER, new Coordinate(7, 3), Direction.VERTICAL);
        System.out.println(p1.remainingShips());
    }

    @Test
    public void placeShips() {
        Player p1 = new Player("Vasile");
        assertEquals(ShipType.AIRCRAFT_CARRIER, p1.nextShipType());

        p1.createShips(ShipType.AIRCRAFT_CARRIER, new Coordinate(1, 1), Direction.HORIZONTAL);
        assertEquals(ShipType.BATTLESHIP, p1.nextShipType());

        p1.createShips(ShipType.BATTLESHIP, new Coordinate(2, 3), Direction.HORIZONTAL);
        assertEquals(ShipType.CRUISER, p1.nextShipType());

        p1.createShips(ShipType.CRUISER, new Coordinate(3, 4), Direction.HORIZONTAL);
        assertEquals(ShipType.DESTROYER, p1.nextShipType());

        p1.createShips(ShipType.DESTROYER, new Coordinate(5, 2), Direction.HORIZONTAL);
        assertEquals(ShipType.DESTROYER, p1.nextShipType());

        p1.createShips(ShipType.DESTROYER, new Coordinate(6, 3), Direction.HORIZONTAL);
        assertEquals(ShipType.SUBMARINE, p1.nextShipType());

        p1.createShips(ShipType.SUBMARINE, new Coordinate(7, 2), Direction.HORIZONTAL);
        assertEquals(ShipType.SUBMARINE, p1.nextShipType());

        p1.createShips(ShipType.SUBMARINE, new Coordinate(8, 3), Direction.HORIZONTAL);
        assertNull(p1.nextShipType());
    }
}