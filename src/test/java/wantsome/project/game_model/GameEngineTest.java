package wantsome.project.game_model;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
import static wantsome.project.game_model.Player.*;

public class GameEngineTest {

    @Test
    public void constructorValidation() {
        try {
            new GameEngine("Gigi", "Gigi");
            fail("Should fail with RuntimeException");
        } catch (RuntimeException e) {
            //expected
        }

        new GameEngine("a", "b");
    }

    @Test
    public void newGameCreation() {
        GameEngine game = new GameEngine("Vlad", "Mihai");
        assertEquals(ShipType.AIRCRAFT_CARRIER, game.getNextShip("Vlad"));

        game.createShip("Vlad", ShipType.AIRCRAFT_CARRIER, 1, 1, Direction.HORIZONTAL);
        game.createShip("Mihai", ShipType.AIRCRAFT_CARRIER, 1, 1, Direction.HORIZONTAL);

        printBoard("Vlad: own board: ", game.myBoard("Vlad"));
        printBoard("Vlad: opponent board, before hits: ", game.opponentBoard("Vlad"));

        for (char[] row : game.opponentBoard("Vlad")) {
            for (char c : row) {
                assertEquals(EMPTY, c);
            }
        }

        assertNull(game.getWinnerName());

        for (int y = 1; y <= 5; y++) {
            game.playHits("Vlad", 1, y);
            assertEquals(HIT, game.myBoard("Mihai")[1][y]);
        }

        game.playHits("Vlad", 2, 1);
        assertEquals(MISS, game.opponentBoard("Vlad")[2][1]);

        printBoard("Vlad: opponent board, after hits", game.opponentBoard("Vlad"));

        System.out.println(game.getWinnerName());
        assertEquals("Vlad", game.getWinnerName());
    }

    private static void printBoard(String msg, char[][] board) {
        System.out.println(msg);
        for (char[] row : board) {
            System.out.println(Arrays.toString(row));
        }
    }
}