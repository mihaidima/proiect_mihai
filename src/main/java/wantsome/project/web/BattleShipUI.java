package wantsome.project.web;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.velocity.VelocityTemplateEngine;
import wantsome.project.game_model.Direction;
import wantsome.project.game_model.GameEngine;

import java.util.*;

import static spark.Spark.*;

public class BattleShipUI {

    private static GameEngine game;
    private static List<String> players = new ArrayList<>();


    public static void configureAndStartServer() {

        staticFileLocation("public");

        get("/main", (req, res) -> getMainPage(req, res));

        get("/login", (req, res) -> showLoginPage());
        post("/login", (req, res) -> handlePostFromLoginPage(req, res));

        get("/wait", (req, res) -> showWaitingPage(req, res));

        get("/prepare", (req, res) -> showPrepareGamePage(req, res));

        get("/changeShipDir", (req, res) -> handleChangeShipDir(req, res));
        get("/placeShip", (req, res) -> handlePlaceShip(req, res));


        get("/play", (req, res) -> showPlayPage(req, res));

        get("/hit", (req, res) -> handleHitRequest(req, res));

        get("/end", (req, res) -> showEndPage(req, res));
        post("/end", (req, res) -> handleLogoutRequest(req, res));

        //get("/logout", (req, res) -> handleLogoutRequest(req, res));

        //for error cases (exceptions in code), will show this error page
        internalServerError("<html><body><h1>Custom 500 handling</h1></body></html>");

        awaitInitialization();
        System.out.println("\nServer started, url: http://localhost:4567/main (use Ctrl+C to stop it)\n");
    }

    private static Object getMainPage(Request request, Response response) {
        getUserName(request, response); //will redirect to login if missing
        response.redirect("/new");
        return response;
    }

    /**
     * Gets the current user name from session; redirects to login page if missing
     */
    private static String getUserName(Request request, Response response) {
        String userFromSession = request.session().attribute("username");
        if (userFromSession == null) {
            response.redirect("/login");
            halt(); //stop rest of processing
        }
        return userFromSession;
    }

    private static Object showLoginPage() {
        return render(Collections.emptyMap(), "login.vm");
    }


    private static Object handlePostFromLoginPage(Request request, Response response) {
        String userNameFromForm = request.queryParams("username"); //read name sent by form submit
        if (players.contains(userNameFromForm)) {
            response.redirect("login");
        } else {
            request.session().attribute("username", userNameFromForm); //save also user name to his permanent session on server!
            players.add(userNameFromForm); //add to list of players
            response.redirect("/wait"); //enter waiting state, will stay there until both players present
        }
        return response;
    }

    private static Object showWaitingPage(Request request, Response response) {
        String user = getUserName(request, response);
        if (players.size() < 2) {
            Map<String, Object> model = new HashMap<>();
            model.put("player", user);
            return render(model, "wait.vm");
        }
        response.redirect("/prepare"); //ok to start the game, redirect to new
        return response;
    }

    private static Object showPrepareGamePage(Request req, Response res) {
        String user = getUserName(req, res);
        //create the game object if needed
        if (game == null) {
            game = new GameEngine(players.get(0), players.get(1));
        }
        Map<String, Object> model = new HashMap<>();
        model.put("player", user);
        model.put("opponent", game.opponentName(user));
        model.put("game", game);
        model.put("direction", getDirectionFromSession(req));
        return render(model, "prepare.vm");
    }

    private static Direction getDirectionFromSession(Request req) {
        String direction = req.session().attribute("direction");
        if (direction == null) {
            direction = Direction.HORIZONTAL.name();
            req.session().attribute("direction", direction);
        }
        return Direction.valueOf(direction);
    }

    private static Object handleChangeShipDir(Request req, Response res) {
        Direction dir = getDirectionFromSession(req);
        dir = dir == Direction.HORIZONTAL ? Direction.VERTICAL : Direction.HORIZONTAL;
        req.session().attribute("direction", dir.name());
        res.redirect("/prepare");
        return res;
    }

    private static Object handlePlaceShip(Request req, Response res) {
        String user = getUserName(req, res);
        int row = Integer.parseInt(req.queryParams("row"));
        int col = Integer.parseInt(req.queryParams("col"));
        try {
            game.createShip(user, game.getNextShip(user), row, col, getDirectionFromSession(req));
            if (game.getNextShip(user) != null) {
                res.redirect("/prepare");
            } else {
                res.redirect("/play");
            }
        } catch (RuntimeException e) {
            System.out.println("Error while placing ships: " + e.getMessage());
            res.redirect("/prepare"); //if an error occured (like error placing ship), remain o same page/step
        }
        return res;
    }

    private static Object showPlayPage(Request req, Response res) {
        String user = getUserName(req, res);
        Map<String, Object> model = new HashMap<>();
        model.put("player", user);
        model.put("game", game);
        return render(model, "play.vm");
    }

    private static Object render(Map<String, Object> model, String templatePath) {
        return new VelocityTemplateEngine().render(new ModelAndView(model, templatePath));
    }

    private static Object handleHitRequest(Request req, Response res) {
        String user = getUserName(req, res);
        String row = req.queryParams("row");
        String col = req.queryParams("col");

        game.playHits(user, Integer.parseInt(row), Integer.parseInt(col));

        if (game.getWinnerName() != null) {
            res.redirect("/end");
        } else {
            res.redirect("/play");
        }
        return res;
    }

    private static Object showEndPage(Request req, Response res) {
        String user = getUserName(req, res);
        Map<String, Object> model = new HashMap<>();
        model.put("player", user);
        model.put("game", game);
        return render(model, "end.vm");
    }

    private static Object handleLogoutRequest(Request req, Response res) {
        //reset session (remove name)
        req.session().removeAttribute("username");
        //then behave like main page
        res.redirect("/main");
        return res;
    }
}
