sort out responsibilities:

    Game
    Keeps 2 Players
    Implements a main playing loop to let attack/fire Players alternately in each round
    Checks if all ships associated at a Players Board are sunken, and the game ends after a round
    Player
    Keeps a Board with a list of their own ships (not visible for other players)
    Keeps a ShadowBoard to track their successful and unsuccessful attack attempts
    Provides a function to fire at the enemy
    In case of a CPU type player keeps a strategy algorithm, which coordinate should be attacked next
    Board

    Keeps the overall size of the gameboard
    Keeps a list of Ships
    Provides a function to add Ships by the associated Player
    Provides a function that allows to attack a specific coordinate and returns a result that indicates

    No hit
    Hit
    Hit and ship sunken
    The result is tracked at the attacking players ShadowBoard and can be used by the strategy algorithm to determine the next coordinate to attack

    Ship

    Keeps its coordinates on the associated Board
    Tracks which coordinates were hit by an attack
    If all possible coordinates were hit, state changes to sunken() == true
    ShadowBoard

    Keeps track of the attack attempt results of the associated Player
    Coordinate

    Something like

    struct Coordinate {
    unsigned x;
    unsigned y;
    std::istream& get(std::istream& is) {
    std::string input;
    if(is >> input & input.size() >= 2) {
    x = toupper(input[0]) - 'A';
    y = input[1] - `0`;
    }
    return is;
    }
    };
    Provides a function to translate a user input like A3, J5, G7, etc. to plain XY coordinates

    Strategy
    Keeps track of the ShadowBoard of the associated Player
    Keeps track of the most recent attack attempt (Coordinate)
    Implements an algorithm to propose the next attack attempt (could be proposed
    to a human player, would be simply attempted by the CPU type player)