package wantsome.project.game_model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Player {

    //marks to use on board array
    public static final char EMPTY = '0';
    public static final char HIT = 'X';
    public static final char MISS = '-';

    private final String name;
    private final char[][] playerBoard = new char[10][10];
    private final List<Ship> shipsPlacement = new ArrayList<>();

    public Player(String name) {
        this.name = name;
        clearBoard();
    }

    private void clearBoard() {
        for (char[] row : playerBoard) {
            Arrays.fill(row, EMPTY);
        }
    }

    public void createShips(ShipType shipType, Coordinate coordinate, Direction direction) {
        this.shipsPlacement.add(new Ship(shipType, coordinate, direction, playerBoard));
    }

    public int remainingShips() {
        return (int) shipsPlacement.stream()
                .filter(ship -> !ship.isSunk())
                .count();
    }

    public char[][] getPlayerBoard() {
        return playerBoard;
    }

    public String getName() {
        return name;
    }


    public List<Ship> getShipsPlacement() {
        return shipsPlacement;
    }

    public ShipType nextShipType() {
        for (ShipType shipType : ShipType.values()) {
            long existingShips = shipsPlacement.stream()
                    .filter(ship -> ship.getShipType() == shipType)
                    .count();
            if (existingShips < shipType.getNumberOfShips()) {
                return shipType;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name) &&
                Arrays.equals(playerBoard, player.playerBoard) &&
                Objects.equals(shipsPlacement, player.shipsPlacement);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, shipsPlacement);
        result = 31 * result + Arrays.hashCode(playerBoard);
        return result;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", playerBoard=" + Arrays.toString(playerBoard) +
                ", shipsPlacement=" + shipsPlacement +
                '}';
    }
}