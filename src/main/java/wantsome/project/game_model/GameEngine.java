package wantsome.project.game_model;

import java.util.HashMap;
import java.util.Map;

import static wantsome.project.game_model.Player.*;

public class GameEngine {

    private final Map<String, Player> players = new HashMap<>();
    private String playerTurn;

    public GameEngine(String namePlayer1, String namePlayer2) {
        if (namePlayer1.equals(namePlayer2)) {
            throw new RuntimeException("Player names should be different: " + namePlayer1);
        }
        this.players.put(namePlayer1, new Player(namePlayer1));
        this.players.put(namePlayer2, new Player(namePlayer2));
        this.playerTurn = namePlayer1;
    }

    public ShipType getNextShip(String name) {
        return players.get(name).nextShipType();
    }

    public void createShip(String name, ShipType shipType, int x, int y, Direction direction) {
        players.get(name).createShips(shipType, new Coordinate(x, y), direction);
    }

    public void playHits(String name, int x, int y) {

        Player opponent = opponent(name);
        for (Ship s : opponent.getShipsPlacement()) {
            if (s.getShipCoordinates().containsKey(new Coordinate(x, y))) {
                s.tryHit(new Coordinate(x, y));
                opponent.remainingShips();
                break;
            }
        }

        char[][] board = opponent.getPlayerBoard();
        board[x][y] = board[x][y] == EMPTY ? MISS : HIT;

        this.playerTurn = opponent.getName();
    }

    public GameProgress gameStatus() {
        if (players.size() < 2) {
            return GameProgress.PLAYER_JOINING;
        }

        boolean allPlaced = players.values().stream().allMatch(p -> p.nextShipType() == null);
        if (!allPlaced) {
            return GameProgress.PLACING_SHIPS;
        }

        boolean ended = players.values().stream().anyMatch(p -> p.remainingShips() == 0);
        return ended ?
                GameProgress.END_GAME :
                GameProgress.HITTING_STAGE;
    }

    public String getWinnerName() {
        for (Player p : players.values()) {
            if (p.remainingShips() == 0) {
                return opponent(p.getName()).getName();
            }
        }
        return null;
    }

    public char[][] opponentBoard(String name) {
        char[][] board = opponent(name).getPlayerBoard();
        char[][] copy = new char[board.length][board.length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                copy[i][j] = board[i][j] == MISS || board[i][j] == HIT ?
                        board[i][j] :
                        EMPTY; //hide rest of cells (may contain marks for rest of ships)
            }
        }
        return copy;
    }

    public char[][] myBoard(String name) {
        return players.get(name).getPlayerBoard();
    }

    public boolean isPlayerTurn(String name) {
        return playerTurn.equals(name);
    }

    public Player player(String name) {
        return players.get(name);
    }

    public Player opponent(String name) {
        return players.values().stream()
                .filter(p -> !p.getName().equals(name))
                .findFirst().orElse(null);
    }

    public String opponentName(String name) {
        return opponent(name).getName();
    }

    @Override
    public String toString() {
        return "GameEngine{" +
                "players=" + players +
                ", playerTurn='" + playerTurn + '\'' +
                '}';
    }
}
