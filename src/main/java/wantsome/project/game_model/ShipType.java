package wantsome.project.game_model;

public enum ShipType {

    AIRCRAFT_CARRIER('A', 1, 5),
    BATTLESHIP('B', 1, 4),
    CRUISER('C', 1, 3),
    DESTROYER('D', 2, 2),
    SUBMARINE('S', 2, 2);

    private char acronym;
    private int numberOfShips;
    private int shipSize;

    ShipType(char acronym, int numberOfShips, int shipSize) {
        this.acronym = acronym;
        this.shipSize = shipSize;
        this.numberOfShips = numberOfShips;
    }

    public char getAcronym() {
        return acronym;
    }

    public int getShipSize() {
        return shipSize;
    }

    public int getNumberOfShips() {
        return numberOfShips;
    }

}
