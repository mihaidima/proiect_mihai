package wantsome.project.game_model;

public enum Direction {
    VERTICAL, HORIZONTAL
}
