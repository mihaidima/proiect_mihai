package wantsome.project.game_model;

public enum GameProgress {

    PLAYER_JOINING,
    PLACING_SHIPS,
    HITTING_STAGE,
    END_GAME
}
