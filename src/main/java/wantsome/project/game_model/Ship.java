package wantsome.project.game_model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static wantsome.project.game_model.Player.EMPTY;

public class Ship {

    private final ShipType shipType;
    private final Map<Coordinate, Boolean> shipCoordinates = new HashMap<>();

    public Ship(ShipType shipType, Coordinate coordinate, Direction direction, char[][] board) {
        this.shipType = shipType;

        if (coordinate.getX() < 0 || coordinate.getX() > 10 || coordinate.getY() < 0 || coordinate.getY() > 10)
            throw new RuntimeException("Starting coordinate is not in the designed board");

        //Ship creation
        if (direction.equals(Direction.HORIZONTAL)) {
            //Direction Horizontal_right
            if ((coordinate.getY() + shipType.getShipSize()) <= board.length) {
                for (int i = 0; i < shipType.getShipSize(); i++) {
                    if (board[coordinate.getX()][coordinate.getY() + i] != EMPTY) {
                        throw new RuntimeException("Not enough space to place the " + shipType);
                    }
                }
                for (int i = 0; i < shipType.getShipSize(); i++) {
                    board[coordinate.getX()][coordinate.getY() + i] = shipType.getAcronym();
                    this.shipCoordinates.put(new Coordinate(coordinate.getX(), coordinate.getY() + i), true);
                }
            } else {
                throw new RuntimeException("Not enough space to place the " + shipType);
            }
        } else { //Battleship placed on Vertical direction
            if (direction.equals(Direction.VERTICAL)) {
                //Direction Vertical_up
                if ((coordinate.getX() + shipType.getShipSize()) <= board.length) {
                    for (int i = 0; i < shipType.getShipSize(); i++) {
                        if (board[coordinate.getX() + i][coordinate.getY()] != EMPTY) {
                            throw new RuntimeException("Not enough space to place the " + shipType);
                        }
                    }
                    for (int i = 0; i < shipType.getShipSize(); i++) {
                        board[coordinate.getX() + i][coordinate.getY()] = shipType.getAcronym();
                        this.shipCoordinates.put(new Coordinate(coordinate.getX() + i, coordinate.getY()), true);
                    }
                } else {
                    throw new RuntimeException("Not enough space to place the " + shipType);
                }
            }
        }
    }

    public void tryHit(Coordinate coordinate) {
        if (shipCoordinates.containsKey(coordinate)) {
            shipCoordinates.replace(coordinate, true, false);
        }
    }

    public boolean isSunk() {
        return shipCoordinates.values().stream()
                .noneMatch(isAlive -> isAlive);
    }

    public Map<Coordinate, Boolean> getShipCoordinates() {
        return shipCoordinates;
    }

    public ShipType getShipType() {
        return shipType;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "shipType=" + shipType +
                ", shipCoordinates=" + shipCoordinates +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return shipType == ship.shipType &&
                Objects.equals(shipCoordinates, ship.shipCoordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shipType, shipCoordinates);
    }
}
