package wantsome.project;

import wantsome.project.web.BattleShipUI;

/**
 * Main class of the app
 */
public class Main {

    public static void main(String[] args) {
        BattleShipUI.configureAndStartServer();
    }
}
